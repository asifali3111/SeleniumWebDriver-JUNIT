This will be a Selenium-Webdriver automation framework using C# in Visual Studios.
The goal will be to include the following featues:
1. Flexible browser choices e.g. IE, Chrome, Firefox, even Phantom JS.
2. Http Response code tests.
3. Database tests via SQL query checks.
4. Tests will be NUNIT tests.